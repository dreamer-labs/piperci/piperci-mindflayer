FROM python:3.7-alpine

RUN pip install pytest PyYAML
ADD tests /tmp/tests

WORKDIR /build

ENTRYPOINT ["pytest", "/tmp/tests/test_stack_yaml.py"]
